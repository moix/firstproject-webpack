/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/css/style.scss":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/css/style.scss ***!
  \*********************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ \"./node_modules/css-loader/dist/runtime/noSourceMaps.js\");\n/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\n/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);\n// Imports\n\n\nvar ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));\n// Module\n___CSS_LOADER_EXPORT___.push([module.id, \".box {\\n  position: relative;\\n  top: 0;\\n  opacity: 1;\\n  float: left;\\n  padding: 60px 50px 40px 50px;\\n  width: 100%;\\n  background: #fff;\\n  border-radius: 10px;\\n  transform: scale(1);\\n  -webkit-transform: scale(1);\\n  -ms-transform: scale(1);\\n  z-index: 5;\\n}\\n.box.back {\\n  transform: scale(0.95);\\n  -webkit-transform: scale(0.95);\\n  -ms-transform: scale(0.95);\\n  top: -20px;\\n  opacity: 0.8;\\n  z-index: -1;\\n}\\n.box:before {\\n  content: \\\"\\\";\\n  width: 100%;\\n  height: 30px;\\n  border-radius: 10px;\\n  position: absolute;\\n  top: -10px;\\n  background: rgba(255, 255, 255, 0.6);\\n  left: 0;\\n  transform: scale(0.95);\\n  -webkit-transform: scale(0.95);\\n  -ms-transform: scale(0.95);\\n  z-index: -1;\\n}\\n\\n.overbox .title {\\n  color: #fff;\\n}\\n.overbox .title:before {\\n  background: #fff;\\n}\\n\\n.title {\\n  width: 100%;\\n  float: left;\\n  line-height: 46px;\\n  font-size: 34px;\\n  font-weight: 700;\\n  letter-spacing: 2px;\\n  color: #ED2553;\\n  position: relative;\\n}\\n.title:before {\\n  content: \\\"\\\";\\n  width: 5px;\\n  height: 100%;\\n  position: absolute;\\n  top: 0;\\n  left: -50px;\\n  background: #ED2553;\\n}\\n\\n.input {\\n  transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -webkit-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -ms-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n}\\n.input label, .input input, .input .spin {\\n  transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -webkit-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -ms-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n}\\n\\n.button {\\n  transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -webkit-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -ms-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n}\\n.button button .button.login button i.fa {\\n  transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -webkit-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -ms-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n}\\n\\n.material-button .shape:before, .material-button .shape:after {\\n  transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -webkit-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -ms-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n}\\n\\n.button.login button {\\n  transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -webkit-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -ms-transition: 300ms cubic-bezier(0.4, 0, 0.2, 1);\\n}\\n\\n.material-button, .alt-2, .material-button .shape, .alt-2 .shape, .box {\\n  transition: 400ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -webkit-transition: 400ms cubic-bezier(0.4, 0, 0.2, 1);\\n  -ms-transition: 400ms cubic-bezier(0.4, 0, 0.2, 1);\\n}\\n\\n.input {\\n  width: 100%;\\n  float: left;\\n}\\n.input label, .input input, .input .spin {\\n  width: 100%;\\n  float: left;\\n}\\n\\n.button {\\n  width: 100%;\\n  float: left;\\n}\\n.button button {\\n  width: 100%;\\n  float: left;\\n}\\n\\n.input, .button {\\n  margin-top: 30px;\\n  height: 70px;\\n}\\n\\n.input {\\n  position: relative;\\n}\\n.input input {\\n  position: relative;\\n}\\n\\n.button {\\n  position: relative;\\n}\\n.button button {\\n  position: relative;\\n}\\n\\n.input input {\\n  height: 60px;\\n  top: 10px;\\n  border: none;\\n  background: transparent;\\n  font-family: \\\"Roboto\\\", sans-serif;\\n  font-size: 24px;\\n  color: rgba(0, 0, 0, 0.8);\\n  font-weight: 300;\\n}\\n.input label {\\n  font-family: \\\"Roboto\\\", sans-serif;\\n  font-size: 24px;\\n  color: rgba(0, 0, 0, 0.8);\\n  font-weight: 300;\\n}\\n\\n.button button {\\n  font-family: \\\"Roboto\\\", sans-serif;\\n  font-size: 24px;\\n  color: rgba(0, 0, 0, 0.8);\\n  font-weight: 300;\\n}\\n\\n.input:before, .input .spin {\\n  width: 100%;\\n  height: 1px;\\n  position: absolute;\\n  bottom: 0;\\n  left: 0;\\n}\\n.input:before {\\n  content: \\\"\\\";\\n  background: rgba(0, 0, 0, 0.1);\\n  z-index: 3;\\n}\\n.input .spin {\\n  background: #ED2553;\\n  z-index: 4;\\n  width: 0;\\n}\\n\\n.overbox .input .spin {\\n  background: white;\\n}\\n.overbox .input:before {\\n  background: rgba(255, 255, 255, 0.5);\\n}\\n\\n.input label {\\n  position: absolute;\\n  top: 10px;\\n  left: 0;\\n  z-index: 2;\\n  cursor: pointer;\\n  line-height: 60px;\\n}\\n\\n.button {\\n  margin-top: 20px;\\n  margin: 40px 0;\\n  overflow: hidden;\\n  z-index: 2;\\n}\\n.button.login {\\n  width: 60%;\\n  left: 20%;\\n}\\n.button.login button {\\n  width: 100%;\\n  line-height: 64px;\\n  left: 0%;\\n  background-color: transparent;\\n  border: 3px solid rgba(0, 0, 0, 0.1);\\n  font-weight: 900;\\n  font-size: 18px;\\n  color: rgba(0, 0, 0, 0.2);\\n}\\n.button button {\\n  width: 100%;\\n  line-height: 64px;\\n  left: 0%;\\n  background-color: transparent;\\n  border: 3px solid rgba(0, 0, 0, 0.1);\\n  font-weight: 900;\\n  font-size: 18px;\\n  color: rgba(0, 0, 0, 0.2);\\n}\\n.button.login {\\n  margin-top: 30px;\\n}\\n.button button {\\n  background-color: #fff;\\n  color: #ED2553;\\n  border: none;\\n}\\n.button.login button.active {\\n  border: 3px solid transparent;\\n  color: #fff !important;\\n}\\n.button.login button.active span {\\n  opacity: 0;\\n  transform: scale(0);\\n  -webkit-transform: scale(0);\\n  -ms-transform: scale(0);\\n}\\n.button.login button.active i.fa {\\n  opacity: 1;\\n  transform: scale(1) rotate(0deg);\\n  -webkit-transform: scale(1) rotate(0deg);\\n  -ms-transform: scale(1) rotate(0deg);\\n}\\n.button.login button i.fa {\\n  width: 100%;\\n  height: 100%;\\n  position: absolute;\\n  top: 0;\\n  left: 0;\\n  line-height: 60px;\\n  transform: scale(0) rotate(-45deg);\\n  -webkit-transform: scale(0) rotate(-45deg);\\n  -ms-transform: scale(0) rotate(-45deg);\\n}\\n.button.login button:hover {\\n  color: #ED2553;\\n  border-color: #ED2553;\\n}\\n.button button {\\n  cursor: pointer;\\n  position: relative;\\n  z-index: 2;\\n}\\n\\n.pass-forgot {\\n  width: 100%;\\n  float: left;\\n  text-align: center;\\n  color: rgba(0, 0, 0, 0.4);\\n  font-size: 18px;\\n}\\n\\n.click-efect {\\n  position: absolute;\\n  top: 0;\\n  left: 0;\\n  background: #ED2553;\\n  border-radius: 50%;\\n}\\n\\n.overbox {\\n  width: 100%;\\n  height: 100%;\\n  position: absolute;\\n  top: 0;\\n  left: 0;\\n  overflow: inherit;\\n  border-radius: 10px;\\n  padding: 60px 50px 40px 50px;\\n}\\n.overbox .title, .overbox .button, .overbox .input {\\n  z-index: 111;\\n  position: relative;\\n  color: #fff !important;\\n  display: none;\\n}\\n.overbox .title {\\n  width: 80%;\\n}\\n.overbox .input {\\n  margin-top: 20px;\\n}\\n.overbox .input input, .overbox .input label {\\n  color: #fff;\\n}\\n.overbox .material-button, .overbox .alt-2 {\\n  display: block;\\n}\\n.overbox .material-button .shape, .overbox .alt-2 .shape {\\n  display: block;\\n}\\n\\n.material-button, .alt-2 {\\n  width: 140px;\\n  height: 140px;\\n  border-radius: 50%;\\n  background: #ED2553;\\n  position: absolute;\\n  top: 40px;\\n  right: -70px;\\n  cursor: pointer;\\n  z-index: 100;\\n  transform: translate(0%, 0%);\\n  -webkit-transform: translate(0%, 0%);\\n  -ms-transform: translate(0%, 0%);\\n}\\n\\n.material-button .shape, .alt-2 .shape {\\n  position: absolute;\\n  top: 0;\\n  right: 0;\\n  width: 100%;\\n  height: 100%;\\n}\\n\\n.material-button .shape:before, .alt-2 .shape:before, .material-button .shape:after, .alt-2 .shape:after {\\n  content: \\\"\\\";\\n  background: #fff;\\n  position: absolute;\\n  top: 50%;\\n  left: 50%;\\n  transform: translate(-50%, -50%) rotate(360deg);\\n  -webkit-transform: translate(-50%, -50%) rotate(360deg);\\n  -ms-transform: translate(-50%, -50%) rotate(360deg);\\n}\\n\\n.material-button .shape:before, .alt-2 .shape:before {\\n  width: 25px;\\n  height: 4px;\\n}\\n\\n.material-button .shape:after, .alt-2 .shape:after {\\n  height: 25px;\\n  width: 4px;\\n}\\n\\n.material-button.active, .alt-2.active {\\n  top: 50%;\\n  right: 50%;\\n  transform: translate(50%, -50%) rotate(0deg);\\n  -webkit-transform: translate(50%, -50%) rotate(0deg);\\n  -ms-transform: translate(50%, -50%) rotate(0deg);\\n}\\n\\nbody {\\n  background-image: url(https://lh4.googleusercontent.com/-XplyTa1Za-I/VMSgIyAYkHI/AAAAAAAADxM/oL-rD6VP4ts/w1184-h666/Android-Lollipop-wallpapers-Google-Now-Wallpaper-2.png);\\n  background-position: center;\\n  background-size: cover;\\n  background-repeat: no-repeat;\\n  min-height: 100vh;\\n  font-family: \\\"Roboto\\\", sans-serif;\\n  overflow: hidden;\\n}\\n\\nhtml {\\n  overflow: hidden;\\n}\\n\\n.materialContainer {\\n  width: 100%;\\n  max-width: 460px;\\n  position: absolute;\\n  top: 50%;\\n  left: 50%;\\n  transform: translate(-50%, -50%);\\n  -webkit-transform: translate(-50%, -50%);\\n  -ms-transform: translate(-50%, -50%);\\n}\\n\\n* {\\n  -webkit-box-sizing: border-box;\\n  -moz-box-sizing: border-box;\\n  box-sizing: border-box;\\n  margin: 0;\\n  padding: 0;\\n  text-decoration: none;\\n  list-style-type: none;\\n  outline: none;\\n}\\n*:after, *::before {\\n  -webkit-box-sizing: border-box;\\n  -moz-box-sizing: border-box;\\n  box-sizing: border-box;\\n  margin: 0;\\n  padding: 0;\\n  text-decoration: none;\\n  list-style-type: none;\\n  outline: none;\\n}\", \"\"]);\n// Exports\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);\n\n\n//# sourceURL=webpack://firstproject/./src/css/style.scss?./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/***/ ((module) => {

eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\nmodule.exports = function (cssWithMappingToString) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = \"\";\n      var needLayer = typeof item[5] !== \"undefined\";\n\n      if (item[4]) {\n        content += \"@supports (\".concat(item[4], \") {\");\n      }\n\n      if (item[2]) {\n        content += \"@media \".concat(item[2], \" {\");\n      }\n\n      if (needLayer) {\n        content += \"@layer\".concat(item[5].length > 0 ? \" \".concat(item[5]) : \"\", \" {\");\n      }\n\n      content += cssWithMappingToString(item);\n\n      if (needLayer) {\n        content += \"}\";\n      }\n\n      if (item[2]) {\n        content += \"}\";\n      }\n\n      if (item[4]) {\n        content += \"}\";\n      }\n\n      return content;\n    }).join(\"\");\n  }; // import a list of modules into the list\n\n\n  list.i = function i(modules, media, dedupe, supports, layer) {\n    if (typeof modules === \"string\") {\n      modules = [[null, modules, undefined]];\n    }\n\n    var alreadyImportedModules = {};\n\n    if (dedupe) {\n      for (var k = 0; k < this.length; k++) {\n        var id = this[k][0];\n\n        if (id != null) {\n          alreadyImportedModules[id] = true;\n        }\n      }\n    }\n\n    for (var _k = 0; _k < modules.length; _k++) {\n      var item = [].concat(modules[_k]);\n\n      if (dedupe && alreadyImportedModules[item[0]]) {\n        continue;\n      }\n\n      if (typeof layer !== \"undefined\") {\n        if (typeof item[5] === \"undefined\") {\n          item[5] = layer;\n        } else {\n          item[1] = \"@layer\".concat(item[5].length > 0 ? \" \".concat(item[5]) : \"\", \" {\").concat(item[1], \"}\");\n          item[5] = layer;\n        }\n      }\n\n      if (media) {\n        if (!item[2]) {\n          item[2] = media;\n        } else {\n          item[1] = \"@media \".concat(item[2], \" {\").concat(item[1], \"}\");\n          item[2] = media;\n        }\n      }\n\n      if (supports) {\n        if (!item[4]) {\n          item[4] = \"\".concat(supports);\n        } else {\n          item[1] = \"@supports (\".concat(item[4], \") {\").concat(item[1], \"}\");\n          item[4] = supports;\n        }\n      }\n\n      list.push(item);\n    }\n  };\n\n  return list;\n};\n\n//# sourceURL=webpack://firstproject/./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/noSourceMaps.js":
/*!**************************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/noSourceMaps.js ***!
  \**************************************************************/
/***/ ((module) => {

eval("\n\nmodule.exports = function (i) {\n  return i[1];\n};\n\n//# sourceURL=webpack://firstproject/./node_modules/css-loader/dist/runtime/noSourceMaps.js?");

/***/ }),

/***/ "./src/css/style.scss":
/*!****************************!*\
  !*** ./src/css/style.scss ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/styleDomAPI.js */ \"./node_modules/style-loader/dist/runtime/styleDomAPI.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/insertBySelector.js */ \"./node_modules/style-loader/dist/runtime/insertBySelector.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js */ \"./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/insertStyleElement.js */ \"./node_modules/style-loader/dist/runtime/insertStyleElement.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/styleTagTransform.js */ \"./node_modules/style-loader/dist/runtime/styleTagTransform.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_style_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! !!../../node_modules/css-loader/dist/cjs.js!../../node_modules/sass-loader/dist/cjs.js!./style.scss */ \"./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/css/style.scss\");\n\n      \n      \n      \n      \n      \n      \n      \n      \n      \n\nvar options = {};\n\noptions.styleTagTransform = (_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default());\noptions.setAttributes = (_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default());\n\n      options.insert = _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default().bind(null, \"head\");\n    \noptions.domAPI = (_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default());\noptions.insertStyleElement = (_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default());\n\nvar update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_style_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"], options);\n\n\n\n\n       /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_style_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"] && _node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_style_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"].locals ? _node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_style_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"].locals : undefined);\n\n\n//# sourceURL=webpack://firstproject/./src/css/style.scss?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/***/ ((module) => {

eval("\n\nvar stylesInDOM = [];\n\nfunction getIndexByIdentifier(identifier) {\n  var result = -1;\n\n  for (var i = 0; i < stylesInDOM.length; i++) {\n    if (stylesInDOM[i].identifier === identifier) {\n      result = i;\n      break;\n    }\n  }\n\n  return result;\n}\n\nfunction modulesToDom(list, options) {\n  var idCountMap = {};\n  var identifiers = [];\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var count = idCountMap[id] || 0;\n    var identifier = \"\".concat(id, \" \").concat(count);\n    idCountMap[id] = count + 1;\n    var indexByIdentifier = getIndexByIdentifier(identifier);\n    var obj = {\n      css: item[1],\n      media: item[2],\n      sourceMap: item[3],\n      supports: item[4],\n      layer: item[5]\n    };\n\n    if (indexByIdentifier !== -1) {\n      stylesInDOM[indexByIdentifier].references++;\n      stylesInDOM[indexByIdentifier].updater(obj);\n    } else {\n      var updater = addElementStyle(obj, options);\n      options.byIndex = i;\n      stylesInDOM.splice(i, 0, {\n        identifier: identifier,\n        updater: updater,\n        references: 1\n      });\n    }\n\n    identifiers.push(identifier);\n  }\n\n  return identifiers;\n}\n\nfunction addElementStyle(obj, options) {\n  var api = options.domAPI(options);\n  api.update(obj);\n\n  var updater = function updater(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap && newObj.supports === obj.supports && newObj.layer === obj.layer) {\n        return;\n      }\n\n      api.update(obj = newObj);\n    } else {\n      api.remove();\n    }\n  };\n\n  return updater;\n}\n\nmodule.exports = function (list, options) {\n  options = options || {};\n  list = list || [];\n  var lastIdentifiers = modulesToDom(list, options);\n  return function update(newList) {\n    newList = newList || [];\n\n    for (var i = 0; i < lastIdentifiers.length; i++) {\n      var identifier = lastIdentifiers[i];\n      var index = getIndexByIdentifier(identifier);\n      stylesInDOM[index].references--;\n    }\n\n    var newLastIdentifiers = modulesToDom(newList, options);\n\n    for (var _i = 0; _i < lastIdentifiers.length; _i++) {\n      var _identifier = lastIdentifiers[_i];\n\n      var _index = getIndexByIdentifier(_identifier);\n\n      if (stylesInDOM[_index].references === 0) {\n        stylesInDOM[_index].updater();\n\n        stylesInDOM.splice(_index, 1);\n      }\n    }\n\n    lastIdentifiers = newLastIdentifiers;\n  };\n};\n\n//# sourceURL=webpack://firstproject/./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertBySelector.js":
/*!********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertBySelector.js ***!
  \********************************************************************/
/***/ ((module) => {

eval("\n\nvar memo = {};\n/* istanbul ignore next  */\n\nfunction getTarget(target) {\n  if (typeof memo[target] === \"undefined\") {\n    var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n    if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n      try {\n        // This will throw an exception if access to iframe is blocked\n        // due to cross-origin restrictions\n        styleTarget = styleTarget.contentDocument.head;\n      } catch (e) {\n        // istanbul ignore next\n        styleTarget = null;\n      }\n    }\n\n    memo[target] = styleTarget;\n  }\n\n  return memo[target];\n}\n/* istanbul ignore next  */\n\n\nfunction insertBySelector(insert, style) {\n  var target = getTarget(insert);\n\n  if (!target) {\n    throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n  }\n\n  target.appendChild(style);\n}\n\nmodule.exports = insertBySelector;\n\n//# sourceURL=webpack://firstproject/./node_modules/style-loader/dist/runtime/insertBySelector.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertStyleElement.js":
/*!**********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertStyleElement.js ***!
  \**********************************************************************/
/***/ ((module) => {

eval("\n\n/* istanbul ignore next  */\nfunction insertStyleElement(options) {\n  var element = document.createElement(\"style\");\n  options.setAttributes(element, options.attributes);\n  options.insert(element, options.options);\n  return element;\n}\n\nmodule.exports = insertStyleElement;\n\n//# sourceURL=webpack://firstproject/./node_modules/style-loader/dist/runtime/insertStyleElement.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\n/* istanbul ignore next  */\nfunction setAttributesWithoutAttributes(styleElement) {\n  var nonce =  true ? __webpack_require__.nc : 0;\n\n  if (nonce) {\n    styleElement.setAttribute(\"nonce\", nonce);\n  }\n}\n\nmodule.exports = setAttributesWithoutAttributes;\n\n//# sourceURL=webpack://firstproject/./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleDomAPI.js":
/*!***************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleDomAPI.js ***!
  \***************************************************************/
/***/ ((module) => {

eval("\n\n/* istanbul ignore next  */\nfunction apply(styleElement, options, obj) {\n  var css = \"\";\n\n  if (obj.supports) {\n    css += \"@supports (\".concat(obj.supports, \") {\");\n  }\n\n  if (obj.media) {\n    css += \"@media \".concat(obj.media, \" {\");\n  }\n\n  var needLayer = typeof obj.layer !== \"undefined\";\n\n  if (needLayer) {\n    css += \"@layer\".concat(obj.layer.length > 0 ? \" \".concat(obj.layer) : \"\", \" {\");\n  }\n\n  css += obj.css;\n\n  if (needLayer) {\n    css += \"}\";\n  }\n\n  if (obj.media) {\n    css += \"}\";\n  }\n\n  if (obj.supports) {\n    css += \"}\";\n  }\n\n  var sourceMap = obj.sourceMap;\n\n  if (sourceMap && typeof btoa !== \"undefined\") {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  options.styleTagTransform(css, styleElement, options.options);\n}\n\nfunction removeStyleElement(styleElement) {\n  // istanbul ignore if\n  if (styleElement.parentNode === null) {\n    return false;\n  }\n\n  styleElement.parentNode.removeChild(styleElement);\n}\n/* istanbul ignore next  */\n\n\nfunction domAPI(options) {\n  var styleElement = options.insertStyleElement(options);\n  return {\n    update: function update(obj) {\n      apply(styleElement, options, obj);\n    },\n    remove: function remove() {\n      removeStyleElement(styleElement);\n    }\n  };\n}\n\nmodule.exports = domAPI;\n\n//# sourceURL=webpack://firstproject/./node_modules/style-loader/dist/runtime/styleDomAPI.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleTagTransform.js":
/*!*********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleTagTransform.js ***!
  \*********************************************************************/
/***/ ((module) => {

eval("\n\n/* istanbul ignore next  */\nfunction styleTagTransform(css, styleElement) {\n  if (styleElement.styleSheet) {\n    styleElement.styleSheet.cssText = css;\n  } else {\n    while (styleElement.firstChild) {\n      styleElement.removeChild(styleElement.firstChild);\n    }\n\n    styleElement.appendChild(document.createTextNode(css));\n  }\n}\n\nmodule.exports = styleTagTransform;\n\n//# sourceURL=webpack://firstproject/./node_modules/style-loader/dist/runtime/styleTagTransform.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _css_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./css/style.scss */ \"./src/css/style.scss\");\n\n$(function() {\n\n    $(\".input input\").focus(function() {\n \n       $(this).parent(\".input\").each(function() {\n          $(\"label\", this).css({\n             \"line-height\": \"18px\",\n             \"font-size\": \"18px\",\n             \"font-weight\": \"100\",\n             \"top\": \"0px\"\n          })\n          $(\".spin\", this).css({\n             \"width\": \"100%\"\n          })\n       });\n    }).blur(function() {\n       $(\".spin\").css({\n          \"width\": \"0px\"\n       })\n       if ($(this).val() == \"\") {\n          $(this).parent(\".input\").each(function() {\n             $(\"label\", this).css({\n                \"line-height\": \"60px\",\n                \"font-size\": \"24px\",\n                \"font-weight\": \"300\",\n                \"top\": \"10px\"\n             })\n          });\n \n       }\n    });\n \n    $(\".button\").click(function(e) {\n       var pX = e.pageX,\n          pY = e.pageY,\n          oX = parseInt($(this).offset().left),\n          oY = parseInt($(this).offset().top);\n \n       $(this).append('<span class=\"click-efect x-' + oX + ' y-' + oY + '\" style=\"margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;\"></span>')\n       $('.x-' + oX + '.y-' + oY + '').animate({\n          \"width\": \"500px\",\n          \"height\": \"500px\",\n          \"top\": \"-250px\",\n          \"left\": \"-250px\",\n \n       }, 600);\n       $(\"button\", this).addClass('active');\n    })\n \n    $(\".alt-2\").click(function() {\n       if (!$(this).hasClass('material-button')) {\n          $(\".shape\").css({\n             \"width\": \"100%\",\n             \"height\": \"100%\",\n             \"transform\": \"rotate(0deg)\"\n          })\n \n          setTimeout(function() {\n             $(\".overbox\").css({\n                \"overflow\": \"initial\"\n             })\n          }, 600)\n \n          $(this).animate({\n             \"width\": \"140px\",\n             \"height\": \"140px\"\n          }, 500, function() {\n             $(\".box\").removeClass(\"back\");\n \n             $(this).removeClass('active')\n          });\n \n          $(\".overbox .title\").fadeOut(300);\n          $(\".overbox .input\").fadeOut(300);\n          $(\".overbox .button\").fadeOut(300);\n \n          $(\".alt-2\").addClass('material-buton');\n       }\n \n    })\n \n    $(\".material-button\").click(function() {\n \n       if ($(this).hasClass('material-button')) {\n          setTimeout(function() {\n             $(\".overbox\").css({\n                \"overflow\": \"hidden\"\n             })\n             $(\".box\").addClass(\"back\");\n          }, 200)\n          $(this).addClass('active').animate({\n             \"width\": \"700px\",\n             \"height\": \"700px\"\n          });\n \n          setTimeout(function() {\n             $(\".shape\").css({\n                \"width\": \"50%\",\n                \"height\": \"50%\",\n                \"transform\": \"rotate(45deg)\"\n             })\n \n             $(\".overbox .title\").fadeIn(300);\n             $(\".overbox .input\").fadeIn(300);\n             $(\".overbox .button\").fadeIn(300);\n          }, 700)\n \n          $(this).removeClass('material-button');\n \n       }\n \n       if ($(\".alt-2\").hasClass('material-buton')) {\n          $(\".alt-2\").removeClass('material-buton');\n          $(\".alt-2\").addClass('material-button');\n       }\n \n    });\n \n });\n \nconst form  = document.getElementById(\"login\");\nconst name = form.elements['name'];\nconst password = form.elements['pass'];\n\n// getting the element's value\n\nform.addEventListener('submit', (event) => {\n   event.preventDefault();\n   let nameVal = name.value;\n   let passwordVal = password.value;\n   console.log(\"name\",nameVal);\n   console.log(\"password\",passwordVal);\n   \n\nlet url = \"http://localhost:4000/login\";\n\nfetch(url,{\n   method: \"POST\",\n     \n    // Adding body or contents to send\n    body: JSON.stringify({\n        username:nameVal,\n        password:passwordVal\n    }),\n     \n    // Adding headers to the request\n    headers: {\n        \"Content-type\": \"application/json; charset=UTF-8\"\n    }\n}).then((response) => response.json()\n  ).then(res=>{\n     console.log(res);\n     if(res == \"user Exists\"){\n      window.location.replace(\"lochpt\");\n     }else{\n      let p = document.getElementById(\"respon\");\n      p.innerHTML = \"Invalid User Name Or Password!\";\n      console.log(p);\n     }\n  })\n  .catch(post => {\n    console.log(post);\n    });\n    \n\n   \n});\n\n//# sourceURL=webpack://firstproject/./src/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;