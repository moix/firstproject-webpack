const path = require("path");
let pages = ["index","home"];
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const config = {
  // entry: pages.reduce((config, page) => {
  //   config[page] = `./src/${page}.js`;
  //   return config;
  // }, {}),
  // output: {
  //   filename: "myBundle.[hash].js",
  //   path: path.resolve(__dirname, "dist"),
  // },
  // // plugins: [
  // //   new HtmlWebpackPlugin(
  // //   { 
  // //     template: "./src/index.html",
  // //     inject: true,
  // //     chunks: ['main'],
  // //     filename: 'index.html'
  // //   }),
    
  // //   new HtmlWebpackPlugin(
  // //     { 
  // //       template: "./src/home.html",
  // //       inject: true,
  // //       chunks: ['home'],
  // //       filename: 'home.html'
  // //     })
  // // ],
  // optimization: {
  //   splitChunks: {
  //     chunks: "all",
  //   },
  // },
  entry: pages.reduce((config, page) => {
    config[page] = `./src/${page}.js`;
    return config;
  }, {}),
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "dist"),
  },
  optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
  plugins: [].concat(
    pages.map(
      (page) =>
        new HtmlWebpackPlugin({
          inject: true,
          template: `./src/${page}.html`,
          filename: `${page}.html`,
          chunks: [page],
        })
    )
  ),
  mode: "development",
  devServer: {
    static: {
        directory: path.join(__dirname, '.dist'),
      },
      compress: true,
      port: 8080,
      hot: true,
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
      {
        test: /\.html$/,
        use: ["html-loader"],
      },
      
     
    ],
  },
};


module.exports = config;
